variables:
  DOCKER_DRIVER: overlay2

  HELM_VERSION: 2.15.1
  KUBERNETES_VERSION: 1.13.12
  GLIBC_VERSION: 2.28-r0

  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"

stages:
  - build
  - test
  - release

build:
  stage: build
  image: docker:stable
  services:
    - docker:stable-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - >-
       docker build
       --build-arg "HELM_VERSION=$HELM_VERSION"
       --build-arg "KUBERNETES_VERSION=$KUBERNETES_VERSION"
       --build-arg "GLIBC_VERSION=$GLIBC_VERSION"
       --tag "$BUILD_IMAGE_NAME" .
    - docker push "$BUILD_IMAGE_NAME"

.test-job: &test-job
  stage: test
  image: "$BUILD_IMAGE_NAME"
  artifacts:
    paths:
      - k3s.log

test-shellcheck:
  stage: test
  image: koalaman/shellcheck-alpine
  script:
    - shellcheck src/bin/auto-deploy

test-shfmt:
  stage: test
  image:
    name: peterdavehello/shfmt:2.6.4
    entrypoint: ["/bin/sh", "-c"]
  script:
    - shfmt -i 2 -ci -l -d src/bin/*

test-dependencies:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
  script:
    - helm version --client
    - tiller -version
    - kubectl version --client

test-kube-domain:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    KUBE_INGRESS_BASE_DOMAIN: example.com
  script:
    - auto-deploy check_kube_domain

test-kube-domain-legacy:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    AUTO_DEVOPS_DOMAIN: example.com
  script:
    - auto-deploy check_kube_domain && expected_error || failed_as_expected

test-kube-domain_error:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
  script:
    - auto-deploy check_kube_domain && expected_error || failed_as_expected

test-download-chart:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
  script:
    - auto-deploy download_chart

test-deploy-name:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    CI_ENVIRONMENT_SLUG: production
  script:
    - name=$(auto-deploy deploy_name "stable")
    - |
      if [[ $name != "production" ]]; then
        echo "$name should equal 'production'"
        exit 1
      fi
    - name=$(auto-deploy deploy_name "canary")
    - |
      if [[ $name != "production-canary" ]]; then
        echo "$name should equal 'production-canary'"
        exit 1
      fi

test-get-replicas:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    CI_ENVIRONMENT_SLUG: production
  script:
    - replicas=$(auto-deploy get_replicas "stable" "100")
    - |
      if [[ $replicas != 1 ]]; then
        echo "$replicas should equal 1"
        exit 1
      fi

test-ensure-namespace:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    KUBE_NAMESPACE: project-123456
  script:
    - download_k3s
    - start_k3s
    - auto-deploy ensure_namespace

test-initialize-tiller:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    KUBE_NAMESPACE: default
  script:
    - download_k3s
    - start_k3s
    - auto-deploy initialize_tiller
    - ps aufx
    - helm ls --host "localhost:44134"

# disabled, doesn't look like k3s supports docker-registry secret
.test-create-secret:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    KUBE_NAMESPACE: default
    CI_REGISTRY: example.com
    CI_DEPLOY_USER: ci-deploy-user
    CI_DEPLOY_PASSWORD: ci-deploy-password
    GITLAB_USER_EMAIL: user@example.com
  script:
    - download_k3s
    - start_k3s
    - auto-deploy create_secret
    - kubectl get secret gitlab-registry -n $KUBE_NAMESPACE

# This template is used for the publish jobs, which do the following:
#   * Check to see if there is a version bump based on
#     [Conventional Commits (v1.0.0-beta.2)](https://www.conventionalcommits.org/en/v1.0.0-beta.2/)
#     See README.md for more information
#   * If there is a new release it will tag the repository with the new release as the `ops-gitlab-net`
#     user

.semantic-release:
  image: node:12
  stage: release
  before_script:
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release $DRY_RUN_OPT -b $CI_COMMIT_REF_NAME
  only:
    variables:
      - $CI_API_V4_URL == "https://gitlab.com/api/v4"

test-create-secret-public-project:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    CI_PROJECT_VISIBILITY: public
    KUBE_NAMESPACE: default
  script:
    - download_k3s
    - start_k3s
    - auto-deploy create_secret
    - kubectl get secret gitlab-registry -n $KUBE_NAMESPACE && expected_error || failed_as_expected

test-persist-environment-url:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    CI_ENVIRONMENT_URL: review-app.example.com
  script:
    - auto-deploy persist_environment_url
    - grep review-app.example.com environment_url.txt

# disabled, k3s does not support helm charts
.test-deploy:
  <<: *test-job
  variables:
    GIT_STRATEGY: none
    CI_APPLICATION_REPOSITORY: "registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image/master/test-dockerfile"
    CI_APPLICATION_TAG: "b359d01bc8c611a2f7b14283cc878dea4a5f85d7"
    CI_ENVIRONMENT_SLUG: production
    CI_ENVIRONMENT_URL: example.com
    CI_PROJECT_PATH_SLUG: "gitlab-org/cluster-integration/auto-build-image"
    CI_PROJECT_VISIBILITY: public
    KUBE_NAMESPACE: default
    KUBE_INGRESS_BASE_DOMAIN: example.com
    POSTGRES_ENABLED: true
    POSTGRES_USER: user
    POSTGRES_PASSWORD: testing-password
    POSTGRES_ENABLED: "true"
    POSTGRES_DB: $CI_ENVIRONMENT_SLUG
    POSTGRES_VERSION: 9.6.2
  script:
    - download_k3s
    - start_k3s
    - auto-deploy initialize_tiller
    - auto-deploy download_chart
    - auto-deploy deploy
    - helm ls --host "localhost:44134"

test-create-application-secret:
  <<: *test-job
  variables:
    KUBE_NAMESPACE: default
    CI_ENVIRONMENT_SLUG: production
    K8S_SECRET_CODE: 12345
  script:
    - download_k3s
    - start_k3s
    - auto-deploy create_application_secret "stable"
    - kubectl get secrets -n $KUBE_NAMESPACE
    - kubectl get secrets production-secret -n $KUBE_NAMESPACE

release-tag:
  stage: release
  image: docker:stable
  services:
    - docker:stable-dind
  script:
    - 'echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY'
    - export ci_image="${CI_REGISTRY_IMAGE}"
    - export ci_image_tag=${CI_COMMIT_TAG:-$CI_COMMIT_SHORT_SHA}
    - echo "Using tag $ci_image_tag for image"
    - docker pull "$BUILD_IMAGE_NAME"
    - docker tag "$BUILD_IMAGE_NAME" $ci_image:latest
    - docker tag "$BUILD_IMAGE_NAME" $ci_image:$ci_image_tag
    - docker push $ci_image:latest
    - docker push $ci_image:$ci_image_tag
  only:
    - tags

before_script:
  - |
    function expected_error() {
      echo "Expected error but exited with $?, failing build!"
      exit 1
    }

    function failed_as_expected() {
      echo "Failed as expected and exited with $?"
    }

    function download_k3s() {
      wget https://github.com/rancher/k3s/releases/download/v0.7.0/k3s
      chmod +x k3s
    }

    function start_k3s() {
      ./k3s server > k3s.log 2>&1 &

      until [ -f /etc/rancher/k3s/k3s.yaml ]; do
        sleep 1
        echo -n .
      done

      export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
      kubectl version
    }

publish:
  extends: .semantic-release
  only:
    refs:
      - master

publish-dryrun:
  extends: .semantic-release
  variables:
    DRY_RUN_OPT: '-d'
  except:
    refs:
      - master
